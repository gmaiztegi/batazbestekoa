import java.util.*;
import java.io.*;

public class Batazbestekoa {

	public static void main(String[] args) throws IOException {
		System.out.println("Idatzu zure zenbakiak intro sakatuz gero eta lerro hutsa utzi amaitzeko:");

		List<Integer> zenbakiLista = zenbakiListaSortu();
		int batazbestekoa = batazbestekoaKalkulatu(zenbakiLista);

		System.out.println("Emaitza hauxe da: "+batazbestekoa);
	}

	private static float batazbestekoaKalkulatu(List<Integer> zenbakiLista) {
		int batura = 0;

		if (zenbakiLista.size() == 0) return 0.0;

		for (Integer zenbakia: zenbakiLista) {
			batura += zenbakia;
		}

		int emaitza = (float)batura / zenbakiLista.size();

		return emaitza;
	}

	private static List<Integer> zenbakiListaSortu() throws IOException {

		BufferedReader irakurtzailea = new BufferedReader(new InputStreamReader(System.in));
		List<Integer> zenbakiLista = new ArrayList<Integer>();

		while (true) {
			try {
				int zenbakia = Integer.parseInt(lerroa);
				zenbakiLista.add(zenbakia);
			} catch (NumberFormatException ex) {
				break;
			}
		}

		return zenbakiLista;
	}
}